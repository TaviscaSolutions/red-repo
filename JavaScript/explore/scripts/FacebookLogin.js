﻿var Explore = Explore || {};

    Explore.FacebookIntegration = {};

    Explore.FacebookIntegration.statusChangeCallback = function (response) {

    if (response.status === 'connected') {
        // Logged into your app and Facebook.
        var id = response.authResponse.userID;
        document.getElementById("userPic").src = "http://graph.facebook.com/" + id + "/picture?type=square";
        var j = 40;
        FB.api(
     "/v1.0/me?fields=name,tagged_places",
     function (response) {

         if (response && !response.error) {

             for (var i = 0; i < response.tagged_places.data.length; i++) {
                 var taggedPlaces = response.tagged_places.data[i].place.name;

                 var elm = '<div class="map-geocoder active" style="top:' + j + 'px;">' + '<span class="category">' + taggedPlaces + '</span>' + '</div>' + '<br />';

                 var cont = document.getElementById("placeView");

                 $(elm).appendTo(cont);
                 j += 35;
             }
         }
     }
 );
    } else if (response.status === 'not_authorized') {

        $('#placeContainer').animate({ width: 'hide' }, 500, function () {
            $('#placeShow').show();
        });
        document.getElementById("userPic").src = "Images/anonymous-Icon.jpg";

    } else {
        document.getElementById("userPic").src = "Images/anonymous-Icon.jpg";
   
        document.getElementById("userPic").src = "Images/anonymous-Icon.jpg";
        $('#placeContainer').animate({ width: 'hide' }, 500, function () {
            $('#placeShow').show();
        });

    }
}

Explore.FacebookIntegration.checkLoginState = function() {
    FB.getLoginStatus(function (response) {
        Explore.FacebookIntegration.statusChangeCallback(response);
    });
}

window.fbAsyncInit = function () {
    FB.init({
        appId: '1444860325791398',
        cookie: true,  // enable cookies to allow the server to access 
        // the session
        xfbml: true,  // parse social plugins on this page
        version: 'v2.0' // use version 2.0
    });

    FB.getLoginStatus(function (response) {
        Explore.FacebookIntegration.statusChangeCallback(response);
    });

    FB.Event.subscribe('auth.logout', function () {
        location.reload();
        document.getElementById("placeContainer").style.display = 'none';
        document.getElementById("userPic").src = "Images/anonymous-Icon.jpg";
    });

    FB.Event.subscribe('auth.login', function () {
        document.getElementById("placeContainer").style.display = 'block';
    });

    
};

// Load the SDK asynchronously
(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));