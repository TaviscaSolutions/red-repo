﻿/**
 * [Return Object]
 */

var Explore = Explore || {};

Explore.filter = Explore.filter || {};


Explore.filter.ReturnObject = function () {
    this.category = null;
    this.displayStatus = null;
}
/**
 * [Filter Object]
 */
Explore.filter.FilterObject=function() {
    var _category;
    var _displayStatus = true;
    /**
    * [initialize filter Object]
    * @param  {[String]} category
    * @param  {[boolean]} displayStatus
    */
    this.initialize = function (category, displayStatus) {
        this._category = category;
        this._displayStatus = displayStatus;
    }
    /**
    * [returns category of current object]
    */
    this.getCategory = function () {
        return this._category
    };
    /**
    * [change the current display status of current object]
    */
    this.changeCategory = function () {
        if (this._displayStatus == true)
            this._displayStatus = false;
        else
            this._displayStatus = true;
    }
    /**
    * [return display status of current object] 
    */
    this.getDisplayStatus = function () {
        return this._displayStatus;
    };
}
Explore.filter.Category=function(){
    this.categories = ["food", "coffee", "outdoors", "drinks", "shops", "arts"];

}
Explore.filter.Filter= function () {
    var _FilterCollection = [];
    var categories = new Explore.filter.Category().categories;
    /**
    * [Set all the filters to the default values]
    */
    this.setAllFilters = function () {
        var counter = 0;
        while (counter < categories.length) {
            _FilterCollection[counter] = new Explore.filter.FilterObject();
            _FilterCollection[counter].initialize(categories[counter], true);
            counter = counter + 1;
        }
    }
    /**
   * [Change the category of filter object based on filter category]
   * @param  {[String]} filterCategory
   * @return {[return Object which contains wheather Marker of filterCategory is to be disabled or not]}
   */
    this.changeCategory = function (filterCategory) {
        var retObject = new Explore.filter.ReturnObject();
        var counter = 0;
        while (counter < categories.length) {
            if (_FilterCollection[counter].getCategory() == filterCategory) {
                _FilterCollection[counter].changeCategory();
                retObject.category = _FilterCollection[counter].getCategory();
                retObject.displayStatus = _FilterCollection[counter].getDisplayStatus();
                return retObject;
            }
            counter = counter + 1;
        }
    }
}