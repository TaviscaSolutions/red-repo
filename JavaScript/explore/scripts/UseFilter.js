﻿
$(document).ready(function () {
    var filter = new Explore.filter.Filter();
    /**
     * [event subscribed on load of body
     *  Set all the Markers to the default i.e, true]
     */
    $('body').on('locationsRecieved.initFilters', function (e) {
        $('#results .map-geocoder').removeClass('inactive').addClass('active');
        filter.setAllFilters();
    });
    /**
    * [event subscribed on load of body
    *  Set all the counts of categories on UI]
    * @param  {[countObject]} e
    */
    $('body').on('initCategoryCount', function (e, returnCount) {
        $('#FoodCnt').html(arguments[1]);
        $('#CoffeeCnt').html(arguments[2]);
        $('#OutdoorsCnt').html(arguments[3]);
        $('#DrinksCnt').html(arguments[4]);
        $('#ShopsCnt').html(arguments[5]);
        $('#ArtsCnt').html(arguments[6]);
        $('#CategoryCnt').html(parseInt(arguments[1]) +
            parseInt(arguments[2]) +
            parseInt(arguments[3]) +
            parseInt(arguments[4]) +
            parseInt(arguments[5]) +
            parseInt(arguments[6])
            );

    var ret=[];
    var counter=0;
        triggerFilterChangeEvent = function (category) {
            retObject = new Explore.filter.ReturnObject();
            retObject.category=category;
            retObject.displayStatus = true;
            ret[counter]=retObject;

            counter=counter+1;
        };

        var appliedFilters = new Explore.filter.Category().categories;

        appliedFilters.forEach(function (item, index) {
            triggerFilterChangeEvent(item);
        });
        $(document.body).trigger('updateMarker', ret);
        
    });


    /**
     * [Event subscribbed on keyPress of '#txtSpecificSearch'
     *   Triggeres 'filter.searchTermChange' event for searching specifically for a particular item]
     * @param  {[]} e
     * @return {[]}
     */
    $('#txtSpecificSearch').on("keypress", function(e) {
        if (e.keyCode == 13) {
            var reg = /^[a-zA-Z0-9]+$/;
            if (reg.test(this.value)&&this.value!="") {
                this.value && $('body').trigger('filter.searchTermChange', this.value);
            }
            else {
               this.value = "[error:Cannot be empty and Contain Special characters!!!]";
            }
        }
    });
    
    /**
     * [Event subscribed on click of '#filterView'
     *  Triggers 'updateMarkers' event with arguments as filter category and display status]
     * @param  {[]} event
     * @return {[]}
     */
    $('#filterView').on('click', function (event) {
        $(this).find("#" + event.target.id).toggleClass("active inactive");
        $(document.body).trigger('updateMarker', filter.changeCategory(event.target.id));
    });
});