var Explore = Explore || {};
Explore.AutoComplete = {};
/**
 * [This function makes an ajax call and obtains the location objects and shows results using the autocomplete plugin]
 */
$(document).ready(function () {
    var loc = new Explore.Locations('http://explore.appacitive.com/locations', { rad: 3, pnum: 1, psize: 75, q: ''});
    loc.initSubscription();

    $(document).ajaxStart(function() {
        $('#place-loader').removeClass('hidden');
    });
    
    $(document).ajaxStop(function() {
        $('#place-loader').addClass('hidden');
    });
    
    $("#txtSearch").autocomplete({

        source: function (request, response) {
            $.ajax({
                url: "http://dev.virtualearth.net/REST/v1/Locations",
                dataType: "jsonp",
                data: {
                    key: "Aj98sZBu_rbzINClCTOS84HwAAJP37mNc-kTj2gsa4NkyU4fOcOqBuHrSU-U71RN",
                    q: request.term
                },
                jsonp: "jsonp",
                success: function (data) {
                    var result = data.resourceSets[0];
                    if (result) {
                        if (result.estimatedTotal > 0) {
                            response($.map(result.resources, function (item) {
                                return {
                                    data: item,
                                    label: item.name + ' (' + item.address.countryRegion + ')',
                                    value: item.name
                                }
                            }));
                        }
                    }
                }
            });
        },
        minLength: 1,
        change: function (event, ui) {
            if (!ui.item)
                $("#txtSearch").val('');
        },
        select: function (event, ui) {
            Explore.AutoComplete.returnCoordinates(ui.item.data);
        }
    });
});


/**
 * [This function returns the co-ordinates of the selected location and triggers the placeChange event]
 * @param  {[object]} item [array of the details of the location]
 * @return {[object]}      [returns the latitude and longitude as a single object]
 */
Explore.AutoComplete.returnCoordinates = function(item) {
    var latLong = { lat: item.point.coordinates[0], lng: item.point.coordinates[1] }
    $(document.body).trigger('placeChange', latLong);
}

/**
 * [This function asks the user for permission to access their current location and returns the coordinates on success]
 * @return {[object]} [contains the current location of the user]
 */
Explore.AutoComplete.geoLocation= function() {
    function showPosition(position) {
        var x = "Latitude: " + position.coords.latitude + position.coords.longitude;
    }

    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    }
}