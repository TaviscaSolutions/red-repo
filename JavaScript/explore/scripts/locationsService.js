/**
 * Following anonymous function scopes the LocationsConstructor constructor.
 * Returns the constructor.
 */

/**
 * Subscription contract details.
 * [ 
 * 	{event: 'filter', expected-data: query(string)},
 * 	{event: 'placeChange', expected-data: location({lat, lng})}
 * ]
 */

/**
 * Publishing contract details.
 * [
 * 	{
 * 		event: 'locationsRecieved', 
 * 		passed-data: locations(Array),
 * 		description: 'this event is published once locations have been fetched'
 * 	}
 * 	{
 * 		event: 'initAPICall'
 * 		passed-data: none,
 * 		description: 'this event is published when api calls are about to be made'
 * 	}
 * ]
 */

var Explore = Explore || {};

Explore.Locations = (function() {

	/**
	 * Creates an object used to call the API
	 * @param {string} endPoint API end point
	 * @param {object} params   query parameters
	 */
	var LocationsConstructor = function(endPoint, params) {
		if (!endPoint || !params) {
			throw new ReferenceError('undefined parameters')
		}
		this.apiEndPoint = endPoint;
		this.queryParams = params;
		this.dataCount = 0;
	}

	/**
	 * Event handler to be called on placeChange event
	 * @param  {object} newLocation Stores latitude and longitude of new location
	 */
	LocationsConstructor.prototype.placeChange = function (newLocation) {
		this.queryParams.lat = newLocation.lat;
		this.queryParams.lng = newLocation.lng;
		$('body').trigger('initAPICall');
		getLocationsData.call(this);
	};

	/**
	 * Event handler to be called on filter event
	 * @param  {string} query terms to search
	 */
	LocationsConstructor.prototype.searchTermChange = function (query) {
		this.queryParams.q = escape(query);
		getLocationsData.call(this);
		if (query) {
			$('body').trigger('initAPICall');
			getLocationsData.call(this);
		}
	};

	/**
	 * Subscribe to the events by filter and autocomplete components
	 * Lots of context gymnastics done here
	 */
	LocationsConstructor.prototype.initSubscription = function () {
		//Subscribe to events. Subscription by locations.
	
		//placeChange published by autocomplete.
		$('body').on('placeChange.getLocationsData', 
			{self: this}, 
			function(event, obj) {
				event.data.self.placeChange(obj)
			});
		//filter.searchTermChange published by filter.
		$('body').on('filter.searchTermChange',
			{self: this},
			function(event, obj) {
				event.data.self.searchTermChange(obj)
			});
	}

	/**
	 * Gets the locations from server.
	 * Produces the side effect on locationsService.data
	 * The API limits page size to maximum of 200 records
	 * Hence the recursive calls.
	 */
	function getLocationsData() {
		var query = buildQueryString.call(this),
			self = this;

		if (this.queryParams.lat === undefined || this.queryParams.lng === undefined) {
			throw new ReferenceError("location undefined");
		}

		$.getJSON(this.apiEndPoint + query)
			.done(function(response) {
				//Store the recieved records
				self.dataCount += response.location.length;
				$('body').trigger('locationsRecieved', Object.create(response.location));
				/**
				 * Check if all the records have been recieved
				 */
				if (self.dataCount < response.pi.totalrecords ) {
					//Call this method again to retrieve rest of the records
					self.queryParams.pnum++;
					getLocationsData.call(self);
				} else {
					//reset the pnum
					self.queryParams.pnum = 1;
					self.dataCount = 0;
				}
			})
			.fail(function() {
				//reset the pnum on error
				self.queryParams.pnum = 1;
				self.dataCount = 0;
				//add error handling here
			});
	};

	/**
	 * Builds a query string to be passed along with
	 * API end point.
	 * @return {string} The query string.
	 */
	function buildQueryString() {
		var query = this.queryParams,
			output;
		output = ('?lat=' + query.lat) +
			('&lng=' + query.lng) + 
			(query.rad !== undefined ? ('&rad=' + query.rad) : '') + 
			(query.pnum !== undefined ? ('&pnum=' + query.pnum) : '') + 
			(query.psize !== undefined ? ('&psize=' + query.psize) : '') + 
			(query.q !== '' ? ('&q=' + query.q) : '');
		return output;
	};

	//Returns the constructor
	return LocationsConstructor;
}());



/**
 * Uncomment the following code to start a basic test of the module.
 */
// $(document).ready(function() {
// 	var f = new Explore.Locations('http://explore.appacitive.com/locations', { rad: 1, pnum: 1, psize: 200, q: 'kharadi'});
// 	f.initSubscription();
// 	$('body').on('locationsRecieved',  function (e, data) {
//     	console.log(Array.prototype.slice.call(arguments, 1)); 
//     });
//  $('body').on('initAPICall',  function (e) {
//     	console.log('api call started'); 
//     });
// 	$('body').trigger('placeChange', {lat: 18.5507626, lng: 73.95021849999999})
// });