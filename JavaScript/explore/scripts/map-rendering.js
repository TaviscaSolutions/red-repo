var Explore = Explore || {};
Explore.markers = Explore.markers || {};
Explore.markers.MapHandler = {
    /**
     * [setupMap Initializes The Map]
     * @param  {DOM Element,Object} uiElement
     * @param  {[type]} options
     * @return {[type]}
     */
    setupMap: function (uiElement, options) {
        var createdMap = new google.maps.Map(uiElement, options);
        return createdMap;
    },

    /**
     * creates marker
     * @param  {Object} options
     * @return {Object} marker
     */

    createMarker: function (options, map) {
        var url;
        /**
          * Switch-case to decide the type of marker to be placed depending upon the category of the place
          */
        switch (options.category) {
            case 'initial':
                url = 'Images/Markers/marker_userlocation.png';
                break;
            case 'food':
                url = 'Images/Markers/marker_food.png';
                break;
            case 'arts':
                url = 'Images/Markers/marker_arts.png';
                break;
            case 'drinks':
                url = 'Images/Markers/marker_drinks.png';
                break;
            case 'shops':
                url = 'Images/Markers/marker_shops.png';
                break;
            case 'coffee':
                url = 'Images/Markers/marker_coffee.png';
                break;
            case 'outdoors':
                url = 'Images/Markers/marker_outdoors.png';
                break;
        }
        var image = {
            url: url
        };
        var marker = new google.maps.Marker({
            position: options.center,
            category: options.category,
            icon: image.url,
            title: options.name,
            map: map,
            visible: options.visible,
            images: options.images,
            name: options.name,
            address: options.description,
            city: options.city,
            country: options.country,
            street: options.street,
            cityState: options.city + ',' + options.country,
        });
        return marker;
    },
};
Explore.markers.Markers = function () {
    this.createdMap = null;
    this.markerArray = [];
    this.infowindow = new google.maps.InfoWindow({
        maxWidth: 500,
    });
    //this.countObject = {
    //    'food': 0,
    //    'coffee': 0,
    //    'outdoors': 0,
    //    'drinks': 0,
    //    'shops': 0,
    //    'arts': 0
    //};
    this.categories = new Explore.filter.Category().categories;
    this.returnCount = [];
    this.clearMarkers = function () {
        var arr = this.markerArray;
        for (var i = 0; i < this.categories.length; i++) {
            this.returnCount[i] = 0;
        }

        for (var i = 0; i < arr.length; i++) {
            arr[i].setMap(null);
        };
    }

    this.initialize = function (locObj) {

        var uiElement = document.getElementById("mapView");
        /**
       * Setup Map Properties For Kharadi(Default location)
       */
        var mapProp = {
            center: new google.maps.LatLng(locObj.lat, locObj.lng),
            zoom: 14,
            panControl: true,
            panControlOptions: {
                position: google.maps.ControlPosition.TOP_RIGHT
            },
            zoomControl: true,
            zoomControlOptions: {
                style: google.maps.ZoomControlStyle.LARGE,
                position: google.maps.ControlPosition.TOP_RIGHT
            },
            mapTypeId: google.maps.MapTypeId.ROADMAP     //ROADMAP,HYBRID,SATELLITE,TERRAIN
        };
        //set up the Map
        this.createdMap = Explore.markers.MapHandler.setupMap(uiElement, mapProp);
        /**
         * Setup Marker Properties For Kharadi
         */
        var mockMarkerOptions = {
            center: new google.maps.LatLng(locObj.lat, locObj.lng),
            category: 'initial',
            title: 'Kharadi'
        };
        this.createdMap.center = mockMarkerOptions.center;
        var marker = Explore.markers.MapHandler.createMarker(mockMarkerOptions, this.createdMap);
    }
    this.initMarkers = function (place) {
        var i = 0,
            self;
        var mockMarkerOptions;
        for (i = 0, k = this.markerArray.length; i < place.length; i++, k++) {
            loc = place[i].location;
            latlongArray = loc.split(",");
            latitude = parseFloat(latlongArray[0], 10);
            longitude = parseFloat(latlongArray[1], 10);
            //this.countObject[place[i].category]++;
            for (j = 0; j < this.categories.length; j++) {
                if (this.categories[j] == place[i].category)
                    break;
            }
            if (j != this.categories.length) {
                if (this.returnCount[j] == undefined) {
                    this.returnCount[j] = 1;
                }
                else {
                    this.returnCount[j]++;
                }
            }
            /**
            * Setting Up the Marker Properties
           */

            mockMarkerOptions = {
                center: new google.maps.LatLng(latitude, longitude),
                category: place[i].category,
                title: place[i].category,
                images: place[i].images,
                name: place[i].name,
                address: place[i].description,
                city: place[i].city,
                country: place[i].country,
                street: place[i].street,
                cityState: place[i].city + ',' + place[i].country,
            };
            /**
              * call to create marker function
              * @type {[type]}
          */
            marker = Explore.markers.MapHandler.createMarker(mockMarkerOptions, this.createdMap);
            this.markerArray[k] = marker;
            this.markerArray[k].setVisible(false);
            self = this;
            google.maps.event.addListener(marker, 'click', (function (marker, i) {
                return function () {
                    // infowindow.setContent(contentString);


                    var source = $("#infoWindowTemplate").html();

                    var template = Handlebars.compile(source);
                    var placeholder = $('.overlay');

                    var serverImage = marker.get('images');


                    var category = marker.get('category');
                    var imagePath;
                    if (serverImage[0] == null || serverImage[0] == '') {
                        imagePath = 'images/' + category + '-icon.png';
                    } else {
                        imagePath = serverImage[0];
                    }

                    var context = {
                        'name': marker.get('name'),
                        'address': marker.get('address'),
                        'street': marker.get('street'),
                        'cityState': marker.get('city') + ',' + marker.get('country'),
                        'photo': imagePath
                    };
                    var html = template(context);
                    self.infowindow.setContent(html);
                    placeholder.append(self.infowindow.open(self.createdMap, marker));
                    var borderShow = $('.overlay').parent().parent().parent().siblings();
                    borderShow.css('border', '3px solid #FF9C24');

                }
            })(marker, i));
        }
        var counter = 0;
        while (counter < this.categories.length) {
            if (this.returnCount[counter] == undefined) {
                this.returnCount[counter] = 0;
            }
            counter++;
        }
        $(document.body).trigger('initCategoryCount', this.returnCount);
    }
    this.showMarkers = function (obj) {
        var self = this;

        obj.forEach(function (elem, index) {
            for (i = 0; i < self.markerArray.length; i++) {
                if (self.markerArray[i].category == elem.category) {
                    self.markerArray[i].setVisible(elem.displayStatus);
                }
            }
        });
    }
}
