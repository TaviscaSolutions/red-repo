
$(document).ready(function () {
    var markers = new Explore.markers.Markers();
    $(document.body).on('placeChange.getMap', function (e, locObj) {
        markers.initialize(locObj);
    });
    $(document.body).on('updateMarker', function (e, filter) {
        var filters = Array.prototype.slice.call(arguments, 1);
        markers.showMarkers(filters);
    });
    $(document.body).on('initAPICall', function (e) {
        markers.clearMarkers();
    });

    $(document.body).on('locationsRecieved.map', function (e) {
        var places = Array.prototype.slice.call(arguments, 1)
        markers.initMarkers(places);
    });
});