﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace AccessTokenService
{
    [ServiceContract]
    public interface IAccessTokenService
    {

        [OperationContract]
        string GetToken();
        [OperationContract]
        bool ValidateToken(string token);
        [OperationContract]
        bool ReturnToken(string token);
    }
}
