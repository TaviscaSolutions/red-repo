﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18063
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ServiceFixtures.AccessTokenService {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="AccessTokenService.IAccessTokenService")]
    public interface IAccessTokenService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IAccessTokenService/GetToken", ReplyAction="http://tempuri.org/IAccessTokenService/GetTokenResponse")]
        string GetToken();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IAccessTokenService/GetToken", ReplyAction="http://tempuri.org/IAccessTokenService/GetTokenResponse")]
        System.Threading.Tasks.Task<string> GetTokenAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IAccessTokenService/ValidateToken", ReplyAction="http://tempuri.org/IAccessTokenService/ValidateTokenResponse")]
        bool ValidateToken(string token);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IAccessTokenService/ValidateToken", ReplyAction="http://tempuri.org/IAccessTokenService/ValidateTokenResponse")]
        System.Threading.Tasks.Task<bool> ValidateTokenAsync(string token);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IAccessTokenService/ReturnToken", ReplyAction="http://tempuri.org/IAccessTokenService/ReturnTokenResponse")]
        bool ReturnToken(string token);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IAccessTokenService/ReturnToken", ReplyAction="http://tempuri.org/IAccessTokenService/ReturnTokenResponse")]
        System.Threading.Tasks.Task<bool> ReturnTokenAsync(string token);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IAccessTokenServiceChannel : ServiceFixtures.AccessTokenService.IAccessTokenService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class AccessTokenServiceClient : System.ServiceModel.ClientBase<ServiceFixtures.AccessTokenService.IAccessTokenService>, ServiceFixtures.AccessTokenService.IAccessTokenService {
        
        public AccessTokenServiceClient() {
        }
        
        public AccessTokenServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public AccessTokenServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public AccessTokenServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public AccessTokenServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public string GetToken() {
            return base.Channel.GetToken();
        }
        
        public System.Threading.Tasks.Task<string> GetTokenAsync() {
            return base.Channel.GetTokenAsync();
        }
        
        public bool ValidateToken(string token) {
            return base.Channel.ValidateToken(token);
        }
        
        public System.Threading.Tasks.Task<bool> ValidateTokenAsync(string token) {
            return base.Channel.ValidateTokenAsync(token);
        }
        
        public bool ReturnToken(string token) {
            return base.Channel.ReturnToken(token);
        }
        
        public System.Threading.Tasks.Task<bool> ReturnTokenAsync(string token) {
            return base.Channel.ReturnTokenAsync(token);
        }
    }
}
